<?php

namespace Managers\ProductPkg;

use Managers\AbstractManager;
use Tannguyen\Elasticsearch\Service\SearchManagerInterface;


/**
 * Class SessionManager Adapter
 * @package Managers
 */
class PageSearchManager extends AbstractManager implements SearchManagerInterface
{
    /**
     * @param $criteria
     */
    public function search($criteria)
    {
        //
    }
}