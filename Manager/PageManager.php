<?php

namespace Managers\PagePkg;

use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Managers\AbstractManager;
use Managers\ManagerInterface;
use Models\Category;
use Models\Image;
use Models\PagePkg\ContentNode;
use Models\PagePkg\Page;

/**
 * Class PageManager
 * @package Managers\PagePkg
 */
class PageManager extends AbstractManager implements ManagerInterface
{
    /**
     * Page Namespace
     * @var
     */
    public $namespace;

    /**
     * @param $slug
     */
    public function loadObjectFromSlug($slug)
    {
       $this->findByNamedSlug($slug);
    }

    /**
     * Get Current Page
     * @return Model
     */
    public function getCurrentPage()
    {
        return $this->model;
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getLatestPages()
    {
        $builder = $this->model->newQuery();
        return $builder->where('namespace' ,'=', $this->model->getNamespace())->orderBy('created_at', 'desc')->paginate(12);
    }

    /**
     * Find Page by named slug
     * @param $slug
     * @return AdsPage|BlogPage|null
     */
    public function findByNamedSlug($slug)
    {
        // if model already loaded
        if ($this->model->slug == $slug) {
            return $this->model;
        }
        // look into database and assign new instance
        return $this->model = $this->model->newQuery()->where(['namespace' => $this->namespace, 'slug' => $slug])->first();
    }

    /**
     * To Json String
     * @param array $data
     * @return string
     */
    public function toJson(array $data)
    {
        return json_encode((object) $data);
    }

    /**
     * Create Page
     * @param $pageData
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createPage($pageData)
    {
        // handle page content
        $this->model->parent_id = null;
        $this->model->namespace = $this->namespace;
        $pageData['page']['nodes']['content'] = $this->sanitize($pageData['page']['nodes']['content']);
        $this->model->nodes = $this->toJson($pageData['page']['nodes']);
        $this->model->ip = $this->request->ip();
        $this->model->slug = SlugService::createSlug(get_class($this->model), 'slug', $pageData['page']['nodes']['title']);
        $this->model->created_by = $this->request->user()->id;
        $this->model->save();
        if (!empty($files = $this->request->file('image_files'))) {
            foreach ($files as $file) {
                /* @var \Illuminate\Http\UploadedFile  $file */
                $newImage = $this->model->newImage();
                $newImage->original_name = $file->getClientOriginalName();
                $newImage->hash_name = hash_image($file);
                $newImage->file_path = get_relative_storage_path();
                $newImage->file_size = $file->getSize();
                $newImage->file_ext  = $file->guessExtension();
                $newImage->created_by = $this->request->user()->id;
                $newImage->file_physical_path = $file->storePubliclyAs(get_physical_storage_path(), $newImage->getFileName(), 'local');
                /* @see \Illuminate\Filesystem\FilesystemManager Storage*/
                $newImage->save();
            }
        }

        // sync categories
        $categoryIds = isset($pageData['categories']) ? Category::getCategoryIds($pageData['categories']) : [];
        $this->model->categories()->sync($categoryIds);
        $this->model->save();
        return $this->model;
    }

    /**
     * Update Page
     * @param $pageData
     * @return Model
     */
    public function updatePage($pageData)
    {
        $pageData['page']['nodes']['content'] = $this->sanitize($pageData['page']['nodes']['content']);
        $this->model->nodes = $this->toJson($pageData['page']['nodes']);
        $this->model->ip = $this->request->ip();
        $this->model->slug = SlugService::createSlug(get_class($this->model), 'slug', $pageData['page']['nodes']['title']);
        $this->model->updated_by = $this->request->user()->id;

        // upload files images
        if (!empty($files = $this->request->file('image_files'))) {
            foreach ($files as $index => $file) {
                /* @var \Illuminate\Http\UploadedFile  $file */
                $newImage = $this->model->newImage();
                $newImage->original_name = $file->getClientOriginalName();
                $newImage->hash_name = hash_image();
                $newImage->file_path = get_relative_storage_path();
                $newImage->file_size = $file->getSize();
                $newImage->file_ext  = $file->guessExtension();
                $newImage->created_by = $this->request->user()->id;
                $newImage->file_physical_path = $file->storePubliclyAs(get_physical_storage_path(), $newImage->getFileName(), 'local');
                /* @see \Illuminate\Filesystem\FilesystemManager Storage*/
                $newImage->save();
            }
        }

        $categoryIds = isset($pageData['categories']) ? Category::getCategoryIds($pageData['categories']) : [];
        $this->model->categories()->sync($categoryIds);

        $this->model->save();
        return $this->model;
    }

    public function deletePage()
    {

    }

    /**
     * Delete Page's Image
     * @param Image $image
     * @return bool|null
     * @throws \Exception
     */
    public function deletePageImage(Image $image)
    {
        if(Storage::disk('local')->exists($image->file_physical_path)) {
            Storage::disk('local')->delete($image->file_physical_path);
        }
        return $image->delete();
    }

    /**
     * Post a comment to Page
     * @param $commentData
     * @return mixed
     */
    public function postComment($commentData)
    {
        $newComment = $this->model->newComment();
        $newComment->content = $this->sanitize($commentData['content']);
        $newComment->ip = $this->request->ip();
        $newComment->created_by = $this->request->user()->id;
        $newComment->save();
        return $newComment;
    }
}