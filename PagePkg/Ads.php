<?php

namespace Models\PagePkg;


use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Models\BaseModel;
use Models\Category;
use Models\UserPkg\User;

/**
 * Class Page
 * @package Models\PagePkg
 */
class Ads extends Page
{
    /**
     * Name Space
     */
    const NAME_SPACE = 'ads';

    /**
     * Static Name Space
     * @var string
     */
    public static $NAME_SPACE = self::NAME_SPACE;

    /**
     * Index Name- Default Value
     * @var string
     */
    public $esIndexName = 'amazingparrot';

    /**
     * Index Type: It can be a generic 'page' - depend on how we want to mix ads with blog or other page inheritances
     * @var string
     */
    public $esIndexType = 'pages';

}
