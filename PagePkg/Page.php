<?php

namespace Models\PagePkg;


use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Support\Facades\Validator;
use Models\BaseModel;
use Models\Category;
use Models\Image;
use Models\UserPkg\User;
use Tannguyen\Elasticsearch\Indexable;

/**
 * Class Page
 * @package Models\PagePkg
 */
class Page extends BaseModel
{
    use Indexable, Sluggable, SluggableScopeHelpers;

	/**
	 * @var string
	 */
	protected $table = 'pages';

	/**
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * @var bool
	 */
	public $timestamps = true;

    /**
     * Page Name Space
     * @var null
     */
	public static $NAME_SPACE = null;

    /**
     * Index Name- Default Value
     * @var string
     */
    public $esIndexName = 'amazingparrot';

    /**
     * Index Type: It can be a generic 'page' - depend on how we want to mix ads with blog or other page inheritances
     * @var string
     */
    public $esIndexType = 'pages';

	/**
	 * Page Types
	 * @var array
	 */
	public static $types = [
		Ads::NAME_SPACE => 'Ads',
        News::NAME_SPACE => 'News',
		Blog::NAME_SPACE => 'Blog'
	];

	/**
	 * @var array
	 */
	protected $fillable = [
		'namespace', 'nodes'
	];

	protected $with = [
		'categories'
	];

	/**
	 * Dates
	 * @var array
	 */
	protected $dates = [
		'created_at', 'updated_at', 'deleted_at'
	];

	/**
	 * Rules
     * Use asterisk (*) for multiple uploads
	 * @var array
	 */
	public $rules = [
        'page.nodes' => 'required|array',
        'page.nodes.title' => 'required|max:240',
        'page.nodes.content' => 'required',
        'image_files' => 'max:4',
        'image_files.*' => 'image|mimes:jpeg,jpg,png,gif|max:2048'
	];


    /**
     * Post Rules
     * @var array
     */
	public $postRules = [
        'namespace' => 'required|max:45',
		'ip' => 'required|ip',
		'parent_id' => 'integer',
		'created_by' => 'required',
        'nodes' => 'required|json',
        'slug' => 'required'
    ];

    /**
     * Overwrite the base model to validate such complicated cases
     * Each Entity need to adapt each validation situation
     * @param $data
     * @param array|null $rules
     * @return mixed|void
     */
	public function validate($data, array $rules = null)
    {
        if(!empty($fileCount = isset($data['image_files']) ? $data['image_files'] : null)) {
            $fileCount = count($data['image_files']);
        }
        if ($uploadedFileCount = $this->images()->count()) {
            $uploadedFileCount = $this->images()->count();
        }
        if ($fileCount + $uploadedFileCount > 4) {
            // invalid this rule
            $this->rules['image_files'] = 'max:0';
        }

        return Validator::make($data, $this->rules);
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => ['source' => ['nodes' => 'title']]    // need for slug service to work
        ];
    }

    /**
	 * Users Relationship
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo(User::class, 'created_by');
	}


	/**
	 * Categories Relationship
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function categories()
	{
		return $this->belongsToMany(Category::class, 'pages_categories', 'page_id', 'category_id');
	}

    /**
     * Comments Relationship
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
	public function comments()
	{
		return $this->morphMany(Comment::class, 'commentable');
	}

    /**
     * Image Relationship - Image stack
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
	public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    /**
     * Parent's Comment Relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent() {
        return $this->belongsTo(self::class, 'parent_id');
    }

    /**
     * Children's Comment Relationship
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children() {
        return $this->hasMany(self::class, 'parent_id');
    }

    /**
     * Get Indexable Fields
     * @return mixed
     */
    public function getIndexableFields()
    {
        return [
            'namespace' => $this->namespace,
            'slug' => $this->slug,
            'nodes' => $this->nodes,
            'image' => $this->getImageByPageInstance()
        ];
    }

    /**
     * Get Index Mapping of this model to Elastic Search
     * @return array
     */
    public function getAutoCompleteMappings()
    {
        return [
            'properties' => [
                'namespace' => [
                    'type' => 'keyword',
                ],
                'slug' => [
                    'type' => 'text',
                    'analyzer' => $this->getAutoSuggestionAnalyzer(),
                    'search_analyzer' => $this->getAutoSuggestSearchTokenizer()
                ],
                'nodes' => [
                    'type' => 'text',
                ],
                'image' => [
                    'type' => 'text',
                ],
            ]
        ];
    }

    /**
     * Factory to get appropriate page instance
     * @return AdsPage|NewsPage|BlogPage
     */
    public function contentNodes()
    {
        switch ($this->namespace) {
            case Ads::NAME_SPACE :
                return $this->adsPage ? $this->adsPage : $this->adsPage = new AdsPage($this);
                break;
            case News::NAME_SPACE :
                return $this->newsPage ? $this->newsPage : $this->newsPage = new NewsPage($this);
                break;
            case Blog::NAME_SPACE :
                return $this->blogPage ? $this->blogPage : $this->blogPage = new BlogPage($this);
                break;
            default:
                return $this;
                break;
        }
    }

    /**
     * Get Page Instance
     * @param $type Page Type
     * @param null $id Page Id
     * @return $this|Ads|Blog|News
     */
    public function getPageInstance($type, $id = null)
    {
        switch ($type) {
            case Ads::NAME_SPACE :
                return !$id ? new Ads() : Ads::find($id);
                break;
            case News::NAME_SPACE :
                return !$id ? new News() : News::find($id);
                break;
            case Blog::NAME_SPACE :
                return !$id ? new Blog() : Blog::find($id);
                break;
            default:
                return $this;
                break;
        }
    }

	/**
     * Create New Comment Associated To This Page
	 * @return Comment
	 */
	public function newComment()
	{
		$comment = new Comment();
		$comment->commentable()->associate($this);
		return $comment;
	}

    /**
     * Create New Comment Associated To This Page
     * @return Image
     */
	public function newImage()
    {
        $image = new Image();
        $image->imageable()->associate($this);
        return $image;
    }

    /**
     * Get Symbolic Image
     * @return string
     */
    public function getSymbolicImage()
    {
        $image = $this->images()->first();
        return $image ? $image->getPath() : '/storage/sample_image.jpg';
    }

    /**
     * Get Image if we load data from Page Model in place of specific Page Type
     * @return string
     */
    public function getImageByPageInstance()
    {
        $pageInstance = $this->getPageInstance($this->namespace, $this->getAttribute('id'));
        return $pageInstance->getSymbolicImage();
    }

    /**
     * Get Author
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->user->getName();
    }

    /**
     * Get Page Categories
     * @return array
     */
    public function getCategories()
    {
        $categories = [];
        foreach ($this->categories as $category) {
            $categories[$category->type] = $category->name;
        }
        return $categories;
    }

    /**
     * Get All Ads
     * @return mixed
     */
    public static function getAllPages()
    {
        return self::where(['namespace' => static::NAME_SPACE])->get();
    }

    /**
     * @return mixed
     */
    public function getNamespace()
    {
        return static::NAME_SPACE;
    }

    /**
     * @return mixed
     */
    public static function getStaticNamespace()
    {
        return static::NAME_SPACE;
    }
}
