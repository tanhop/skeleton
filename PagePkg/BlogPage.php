<?php

namespace Models\PagePkg;


/**
 * Class BlogPage Object
 * @package Managers\PagePkg
 */
class BlogPage extends PageAbstract
{

    /**
     * Page Content
     * @var
     */
    public $title;

    /**
     * Page Content
     * @var
     */
    public $content;

    /**
     * Allow Display This Page
     * @var
     */
    public $allowDisplay;

    /**
     * Allow Like This Page
     * @var
     */
    public $allowLike;

    /**
     * Allow Comment This Page
     * @var
     */
    public $allowComment;

    /**
     * Allow Share This Page
     * @var
     */
    public $allowShare;

    /**
     * AdsPage constructor.
     * @param $pageInstance
     */
    public function __construct(Page $pageInstance)
    {
        parent::__construct($pageInstance);
        $this->loadPageContent();
    }

    /**
     * Load Page Content
     * @return mixed|void
     */
    public function loadPageContent()
    {
        $pageContent = $this->toArray($this->pageInstance->nodes);
        foreach ($pageContent as $key => $property) {
            $this->$key = $property;
        }
    }
}