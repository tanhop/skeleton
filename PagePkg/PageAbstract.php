<?php
/**
 * Created by PhpStorm.
 * User: HopEm
 * Date: 3/12/2018
 * Time: 8:32 PM
 */
namespace Models\PagePkg;

use App\Helpers\ObjectProperties;

/**
 * Class PageAbstract
 * @package Managers\PagePkg
 */
abstract class PageAbstract
{
    use ObjectProperties;

    /**
     * Page instance
     * @var
     */
    protected $pageInstance;

    /**
     * Page id
     * @var
     */
    public $id;

    /**
     * Parent's page id
     * @var
     */
    public $parent_id;

    /**
     * Page namespace
     * @var
     */
    public $namespace;

    /**
     * Page Content
     * @var
     */
    public $nodes;

    /**
     * Page ip
     * @var
     */
    public $ip;

    /**
     * Page's User id
     * @var
     */
    public $created_by;

    /**
     * Page Slug
     * @var
     */
    public $slug;

    /**
     * @var
     */
    public $created_at;

    /**
     * PageAbstract constructor.
     * @param $pageInstance
     */
    public function __construct($pageInstance)
    {
        $this->pageInstance = $pageInstance;
        $this->loadPage();
    }

    /**
     * How we load Page Content
     * @return mixed
     */
    abstract function loadPageContent();

    /**
     * @return mixed
     */
    public function getInstance()
    {
        return $this->pageInstance;
    }

    /**
     * To Json String
     * @param array $data
     * @return string
     */
    public function toJson(array $data)
    {
        return json_encode((object) $data);
    }

    /**
     * To Array from Json String
     * @param $jsonData
     * @return mixed
     */
    public function toArray($jsonData)
    {
        return json_decode($jsonData, true);
    }

    /**
     * Load Page
     */
    public function loadPage()
    {
        $properties = $this->pageInstance->getAttributes();
        foreach ($properties as $key => $property) {
            $this->$key = $this->pageInstance->$key;
        }
    }

}