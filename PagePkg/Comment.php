<?php

namespace Models\PagePkg;

use Models\BaseModel;
use Models\UserPkg\User;

class Comment extends BaseModel
{
	/**
	 * @var string
	 */
	protected $table = 'comments';

	/**
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * @var bool
	 */
	public $timestamps = true;

	/**
	 * @var array
	 */
	protected $fillable = [
		'content'
	];

	/**
	 * Dates
	 * @var array
	 */
	protected $dates = [
		'created_at', 'updated_at','deleted_at'
	];

	/**
	 * Rules
	 * @var array
	 */
	public static $rules = [
		'content' => 'required',
		'ip' => 'required|ip',
		'user_id' => 'required|integer',
		'page_id' => 'required|integer',
		'parent_id' => 'integer',
	];

    /**
     * Morph To Product, Page
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function commentable()
    {
        return $this->morphTo();
    }

    /**
     * Users Relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
	public function user() {
		return $this->belongsTo(User::class, 'created_by');
	}

	/**
	 * Parent's Comment Relationship
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function parent() {
		return $this->belongsTo(self::class, 'parent_id');
	}

	/**
	 * Children's Comment Relationship
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function children() {
		return $this->hasMany(self::class, 'parent_id');
	}

}
